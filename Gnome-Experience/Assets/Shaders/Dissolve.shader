﻿Shader "Custom/Dissolve" {
	Properties {
		_DiffuseColor("Diffuse Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_DissolveColor("Dissolve Color", Color) = (1.0, 0.0, 0.0, 1.0)
		_Amount("Dissolve Amount", Range(0.0, 1.0)) = 0.0
		_Width("Dissolve Width", Range(0.0, 3.0)) = 1.0
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalTex("Normal Texture", 2D) = "bump" {}
		_DissolveTex("Dissolve Texture", 2D) = "black" {}
	}
	SubShader {
     	Tags { "Queue" = "Transparent" }
		//Cull Off
     	Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma surface surf Lambert alpha

		float4 _DiffuseColor;
		float4 _DissolveColor;
		sampler2D _MainTex;
		sampler2D _NormalTex;
		sampler2D _DissolveTex;
		float _Width;
		float _Amount;

		struct Input {
			float2 uv_MainTex : TEXCOORD0;
			float2 uv_DissolveTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half3 dissolve = tex2D(_DissolveTex, IN.uv_DissolveTex).rgb;
			half4 c = tex2D (_MainTex, IN.uv_MainTex) * _DiffuseColor;
			half3 clear = half3(0.0, 0.0, 0.0);
			int isClear = int(dissolve.r - (_Amount + _Width) + 0.99);
			
			clear = lerp(_DissolveColor, clear, isClear);
			
			o.Albedo = lerp(c.rgb, clear, int(dissolve.r - _Amount + 0.99));
			o.Normal = UnpackNormal(tex2D(_NormalTex, IN.uv_MainTex));
			o.Alpha = lerp(1.0, 0.0, int(dissolve.r - (_Amount + _Width) + 0.99));
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
