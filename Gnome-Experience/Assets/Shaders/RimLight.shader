﻿Shader "Custom/RimLight" {
	Properties {
		_DiffColor("Diffuse Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_RimColor("Rim Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_RimPower("Rim Power", Range(0.5, 8.0)) = 3.0
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalTex("Normal Texture", 2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		sampler2D _NormalTex;
		float4 _DiffColor;
		float4 _RimColor;
		float _RimPower;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			float2 uv = IN.uv_MainTex;
			half4 c = tex2D (_MainTex, uv) * _DiffColor;
			half3 normal = UnpackNormal(tex2D(_NormalTex, uv));
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), normal));
			o.Albedo = c.rgb;
			o.Normal = normal;
			o.Emission = _RimColor.rgb * pow(rim, _RimPower);
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
