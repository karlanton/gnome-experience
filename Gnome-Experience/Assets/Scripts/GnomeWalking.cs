﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GnomeWalking : MonoBehaviour
{

    public Transform Target;
    public float MinDistance;
    public float ShrinkSpeed = 0.5f;
	public float DeathTimer = 4.0f;
    public float DissolveTime = 1.0f;
    public ParticleSystem DeathParticle;
    public Animator AnimatorComp;
	
    public bool Shrink { get; set; }

    Vector3 _originScale;
    bool DoUpdate = true;
	private float _currentTime;
	private GameObject _instantiatedParticle;

    void Start()
    {
		_currentTime = DeathTimer;
        _originScale = transform.localScale;
        Target = WalkerTarget.Target.transform;
        MoverAmbience.RegisterMover(this);
        AnimatorComp.Play("Walk");
        StartCoroutine(WalkTowardsPlayer());
    }

    IEnumerator WalkTowardsPlayer()
    {
        Vector2 startPos = new Vector2(transform.position.x, transform.position.z);
        float startTime = Time.time;
        Vector2 endPos = new Vector2(Target.position.x, Target.position.z);
        float startDistance = Vector2.Distance(startPos, endPos);

        Vector2 pos2D;
        Vector2 targetPos2D;

        float distance;
        do
        {
            float time = Time.time - startTime;
            RaycastHit hit;
            if (Physics.Raycast(new Vector3(transform.position.x, 0.4f, transform.position.z), -Vector3.up, out hit) == false)
            {
                Debug.LogError("Failed to raycast gnome " + gameObject + " " + transform.position, gameObject);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
            }
						
            pos2D = new Vector2(transform.position.x, transform.position.z);
            targetPos2D = new Vector2(Target.position.x, Target.position.z);
            Vector2 dir = targetPos2D - pos2D;
            transform.forward = new Vector3(dir.x, 0, dir.y);
            if (Shrink)
                AnimatorComp.Play("Death");
            else
                AnimatorComp.Play("Walk");
            yield return null;
            distance = Vector2.Distance(pos2D, targetPos2D);
            Vector3 scale = Vector3.Lerp(Vector3.one * 0.8f, Vector3.one * 2.8f, 1 - Mathf.Clamp01(distance / startDistance));
            AnimatorComp.transform.localScale = scale;
        } while (distance > MinDistance);
        GameLogic.GnomeReached();
        Object.Destroy(gameObject);
    }


    void Update()
    {
        if (!DoUpdate)
            return;
        if (AnimatorComp.deltaPosition.magnitude > 0)
        {
            transform.position += AnimatorComp.deltaPosition;
            AnimatorComp.transform.localPosition = Vector3.zero;
        }

        if (Shrink)
		{
			_currentTime -= Time.deltaTime;
			if (_currentTime <= DeathTimer / 2f)
			{
				if (DeathParticle != null && _instantiatedParticle == null)
				{
					audio.Play();
					_instantiatedParticle = Instantiate(DeathParticle.gameObject, transform.position, transform.rotation) as GameObject;
				}
			}
            if (_currentTime <= 0f)
            {
                DoUpdate = false;
                StartCoroutine(Dissolve());
            }
        }
        else if (!Shrink && _currentTime <= DeathTimer)
        {
			_currentTime += Time.deltaTime;
        }
    }

    IEnumerator Dissolve()
    {
        AnimatorComp.Play("Death");
        float time = DissolveTime;

        Renderer r = GetComponentInChildren<Renderer>();

        while (time > 0.0f)
        {
            float t = time / DissolveTime;
            r.material.SetFloat("_Amount", t);
            time -= Time.deltaTime;
            yield return null;
        }

        time = 0.5f;
        float w = r.material.GetFloat("_Width");
        while (w > 0.0f)
        {
            w -= Time.deltaTime;
            r.material.SetFloat("_Width", w);
            time -= Time.deltaTime;
            yield return null;
        }
        /*while (AnimatorComp.GetCurrentAnimatorStateInfo (0).IsName ("Destroy") == false) {
						yield return null;
				}*/
        GameLogic.AddScore(1);
	  
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        MoverAmbience.UnregisterMover(this);
    }
}
