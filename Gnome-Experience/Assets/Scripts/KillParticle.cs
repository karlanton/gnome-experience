﻿using UnityEngine;
using System.Collections;

public class KillParticle : MonoBehaviour 
{
	void Update()
	{
		if (!particleSystem.isPlaying)
		{
			Destroy(gameObject);
		}
	}
}
