﻿using UnityEngine;
using System.Collections;

public class DropSound : MonoBehaviour {

	public AudioClip[] SplashSounds;

	private int particlecount;
	private bool cooldown = false;

	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
		particlecount = particleSystem.particleCount;
		if (particlecount > 2 && !cooldown) {
			audio.clip = SplashSounds [Random.Range (0, SplashSounds.Length - 1)];
			audio.Play();
			cooldown = true;
				}
		if (particlecount == 0) {
			cooldown = false;
				}
	}
}
