﻿using UnityEngine;
using System.Collections;

public class GameLogic : MonoBehaviour 
{
	public enum GameSate
	{
		Intro,
		Cave,
		Lost
	}
	public Animation IntroAnimation;
	public int MaxReachedGnomes = 5;
	public float IntroTimer = 3.0f;
	public static GameSate State = GameSate.Intro;
	public static int Score;
	public static int ReachedGnomes;

	public static void AddScore(int score)
	{
		Score += score;
	}

	public static void GnomeReached()
	{
		ReachedGnomes += 1;
	}

	public void Update()
	{
		if (ReachedGnomes >= MaxReachedGnomes && State != GameSate.Lost)
		{
			State = GameSate.Lost;
		}

		if (State == GameSate.Lost && Input.GetKey(KeyCode.Space))
		{
			Debug.Log("RESSET");
			Reset();
		}
	}

	void Reset()
	{
		if (IntroAnimation != null)
		{
			Application.LoadLevel(Application.loadedLevel);
		}
	}

	public void Start()
	{
		StartCoroutine(DoIntro());
	}

	IEnumerator DoIntro()
	{
		float t = IntroTimer;
		if (IntroAnimation != null)
		{
			while (IntroAnimation.isPlaying)
			{
				yield return null;
			}
		}

		State = GameSate.Cave;
	}
}
