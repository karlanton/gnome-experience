﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
	#region Members

	public bool UseSpotlightAngle = false;
	public float HitAngle = 20.0f;
	public float KillDistance = 3f;

	public float BreathingInterval = 3.0f;
	public AudioSource[] BreathingSounds;

	private OVRCameraRig _cameraRig;
	private bool _cooldown = false;

	AudioSource _currentSource;

	#endregion

	#region Properties

	public new Transform transform { get; private set; }
	public new GameObject gameObject { get; private set; }

	public Transform CameraRigLeftEye 
	{
		get
		{
			return _cameraRig.leftEyeAnchor;
		}
	}

	public Transform CameraRigRightEye 
	{
		get
		{
			return _cameraRig.rightEyeAnchor;
		}
	}

	public Transform CameraRigCenterEye
	{
		get
		{
			return _cameraRig.centerEyeAnchor;
		}
	}
	#endregion

	void Awake()
	{
		_currentSource = GetRandomSource(BreathingSounds);

		transform = base.transform;
		gameObject = base.gameObject;
		_cameraRig = GetComponent<OVRCameraRig>();
	}

	AudioSource GetRandomSource(AudioSource[] sources)
	{
		return sources[Random.Range(0, sources.Length - 1)];
	}

	void Update()
	{
		float hitAngle = UseSpotlightAngle ? CameraRigCenterEye.GetComponentInChildren<Light>().spotAngle : HitAngle;
		hitAngle /= 2f;

		GnomeWalking[] gnomes = FindObjectsOfType<GnomeWalking>() as GnomeWalking[];
		
		GnomeWalking[] spottedGnomes = GetSpottedGnomes(gnomes, hitAngle);

		foreach (GnomeWalking gnome in gnomes)
		{
			gnome.Shrink = false;
		}

		foreach (GnomeWalking gnome in spottedGnomes)
		{
			gnome.Shrink = true;
		}

		if (_currentSource != null && !_currentSource.isPlaying && !_cooldown)
		{
			_currentSource.Play();
			_cooldown = true;
			StartCoroutine(SoundTimer());
		}
	}

	private GnomeWalking[] GetSpottedGnomes(GnomeWalking[] gnomes, float hitAngle)
	{
		List<GnomeWalking> spotted = new List<GnomeWalking>();
		foreach (GnomeWalking gnome in gnomes)
		{
			Ray ray = new Ray(CameraRigCenterEye.position, CameraRigCenterEye.forward);
			Vector3 gnomePos = gnome.transform.position;
			Vector3 closestPointOnRay = ClosestPointOnRay(gnomePos, ray);
			Vector3 closestPointOnBounds = gnome.GetComponentInChildren<Collider>().ClosestPointOnBounds(closestPointOnRay);


			if (Vector3.Distance(closestPointOnRay, CameraRigCenterEye.position) < KillDistance)
			{
				Vector3 v1 = closestPointOnRay - CameraRigCenterEye.position;
				Vector3 v2 = closestPointOnBounds - CameraRigCenterEye.position;

				float angle = Vector3.Angle(v1, v2);
				if (angle < hitAngle)
				{
					spotted.Add(gnome);
				}
			}
		}

		return spotted.ToArray();
	}

	Vector3 ClosestPointOnRay(Vector3 p, Ray r)
	{
		Vector3 px = p - r.origin;
		Vector3 dir = r.direction.normalized;

		float s = Vector3.Dot(px, dir);
		Vector3 closestPoint = dir * s;

		return r.origin + closestPoint;
	}

	IEnumerator SoundTimer()
	{
		yield return new WaitForSeconds(_currentSource.clip.length);

		_currentSource = GetRandomSource(BreathingSounds);

		yield return new WaitForSeconds(Random.Range(1f, BreathingInterval));
		_cooldown = false;
	}
}
