﻿using UnityEngine;
using System.Collections;

public class MoverSpawnPoint : MonoBehaviour {

  #if UNITY_EDITOR
  [UnityEditor.Callbacks.PostProcessScene]
  [UnityEditor.MenuItem("Postprocess/Fix spawn references")]
  static void FixSpawnReferences()
  {
    var objects = Object.FindObjectsOfType(typeof(MoverSpawnPoint)) as MoverSpawnPoint[];
    var moverSpawning = Object.FindObjectOfType(typeof(MoverSpawning)) as MoverSpawning;
    moverSpawning.SpawnPoints = objects;
  }
      #endif
// Update is called once per frame
	void Update () {
	
	}
}
