﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent (typeof(AudioSource))]
public class MoverAmbience : MonoBehaviour
{
		[System.Serializable]
		public struct AmbienceLevel
		{
				public float Radius;
				public float FadeInTime;
				public AnimationCurve Volume;
				public float FadeOutTime;
				public AudioClip Audio;
		}

		struct AmbienceLevelState
		{
				public int NumMoversInLevel;
				public AudioSource AudioSource;
				public bool Fading;
				public float FadeT;
				
		}

		public AmbienceLevel[] Levels;
		AmbienceLevelState[] _LevelsState;
		static 		List<GnomeWalking> _Gnomes;

		public static void RegisterMover (GnomeWalking mover)
		{
				_Gnomes.Add (mover);
		}

		public static void UnregisterMover (GnomeWalking mover)
		{
				_Gnomes.Remove (mover);
		}
		// Use this for initialization
		void Awake ()
		{
				if (_Gnomes == null)
						_Gnomes = new List<GnomeWalking> ();
				_LevelsState = new AmbienceLevelState[Levels.Length];
				for (int i = 0; i < _LevelsState.Length; i++) {
						_LevelsState [i].AudioSource = gameObject.AddComponent<AudioSource> ();
						_LevelsState [i].AudioSource.clip = Levels [i].Audio;
						_LevelsState [i].AudioSource.loop = true;
				}
		}

		void Update ()
		{
				for (int i = 0; i < _LevelsState.Length; i++) {
						_LevelsState [i].NumMoversInLevel = 0;
				}
				WalkerTarget target = WalkerTarget.Target;

				foreach (var gnome in _Gnomes) {
						float distance = Vector3.Distance (target.transform.position, gnome.transform.position);
						for (int i = 0; i < _LevelsState.Length; i++) {
								if (Levels [i].Radius > distance)
										_LevelsState [i].NumMoversInLevel++;
						}
				}

				for (int i = 0; i < Levels.Length; i++) {
						var state = _LevelsState [i];
						if (state.NumMoversInLevel == 0) {
								// should be fading out or stopped
								float deltaT = Time.deltaTime / Levels [i].FadeOutTime;
								if (state.AudioSource.isPlaying) {
										if (state.Fading == false) {
												state.Fading = true;
												state.FadeT = 1f;
										} else {
												state.FadeT -= deltaT; 
												if (state.FadeT <= 0) {
														state.Fading = false;
														state.FadeT = 0f;
														state.AudioSource.Stop ();
												}
										}
								}
								state.AudioSource.volume = Levels [i].Volume.Evaluate (state.FadeT);
						} else {
								float deltaT = Time.deltaTime / Levels [i].FadeOutTime;
								// should be fading in or playing
								if (state.AudioSource.isPlaying == false && state.Fading == false) {
										state.Fading = true;
										state.FadeT = 0f;
								} else if (state.AudioSource.isPlaying && state.Fading) {
										state.FadeT += deltaT;

										if (state.FadeT >= 1f) {
												state.Fading = false;
												state.FadeT = 1f;
										}
								}

								state.AudioSource.volume = Levels [i].Volume.Evaluate (state.FadeT);
								if (state.AudioSource.isPlaying == false) {
										state.AudioSource.Play ();
								}
						}
						_LevelsState [i] = state;
				}
		}
}
