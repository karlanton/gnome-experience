﻿using UnityEngine;
using System.Collections;

public class MonsterMouth : MonoBehaviour {
	
	public AudioClip [] sounds;

	private bool cooldown;
	public float interval =3f;
	private float startdelay;

	// Use this for initialization
	void Start () {
		startdelay = Random.Range (0, interval);
		audio.clip = GetRandomClip (sounds);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!audio.isPlaying && !cooldown && startdelay <= 0f) {
			audio.Play ();
			cooldown =true;
			StartCoroutine(SoundTimer());

				}
		if (startdelay > 0) {
						startdelay -= Time.deltaTime;
				}
	}

	AudioClip GetRandomClip(AudioClip[] Array){
		return Array [Random.Range (0, Array.Length - 1)];
		}

	IEnumerator SoundTimer(){


		yield return new WaitForSeconds (audio.clip.length);

		audio.clip = GetRandomClip (sounds);
		yield return new WaitForSeconds (Random.Range (1F, interval));
		cooldown = false;
		}
}
