﻿using UnityEngine;
using System.Collections;

public class MoverSpawning : MonoBehaviour
{
		struct SpawnPointState
		{
				public    MoverSpawnPoint Point;
				public float CooldownEndTime;
		}

		public MoverSpawnPoint[] SpawnPoints;
		public GameObject WalkerPrefab;
		public float SpawnPointCooldown = 10f;
		public float SpawnFrequencyMin = 3f;
		public float SpawnFrequencyMax = 8f;
		private SpawnPointState[] _SpawnStates;
		private float _NextSpawn;

		void Start ()
		{
				_SpawnStates = new SpawnPointState[SpawnPoints.Length];
				for (int i = 0; i < _SpawnStates.Length; i++) {
						_SpawnStates [i].Point = SpawnPoints [i];
				}
		}
	
		void Update ()
		{
			if (GameLogic.State != GameLogic.GameSate.Cave)
				return;

				if (_NextSpawn < Time.time) {
						_NextSpawn = Time.time + Random.Range (SpawnFrequencyMin, SpawnFrequencyMax);
						int prefabsOffCooldown = 0;
						for (int i = 0; i < _SpawnStates.Length; i++) {
								if (_SpawnStates [i].CooldownEndTime < Time.time)
										prefabsOffCooldown++;
						}
						if (prefabsOffCooldown == 0) {
								Debug.LogError ("Tried to spawn a mover when no spawn points were off cooldown");
								return;
						}
						int randomIndex = Random.Range (0, prefabsOffCooldown);
						int indexIter = 0;
						for (int i = 0; i < _SpawnStates.Length; i++) {
								var state = _SpawnStates [i];
								if (state.CooldownEndTime < Time.time) {
										if (i == randomIndex) {
												GameObject.Instantiate (WalkerPrefab, state.Point.transform.position, state.Point.transform.rotation);
												state.CooldownEndTime = Time.time + SpawnPointCooldown;
												_SpawnStates [i] = state;
												break;
										}
										indexIter++;
								}
						}
				}
		}
}
